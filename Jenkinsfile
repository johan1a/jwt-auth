def label = "worker-${UUID.randomUUID().toString()}"

podTemplate(label: label,
  serviceAccount: 'jenkins-master',
  containers: [
      containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true),
      containerTemplate(name: 'clojure', image: 'clojure', ttyEnabled: true),
      containerTemplate(name: 'kubectl', image: 'lachlanevenson/k8s-kubectl:v1.8.8', command: 'cat', ttyEnabled: true),
],
volumes: [
  hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
]) {
  timeout(time: 30, unit: 'MINUTES') {
    node(label) {
      def myRepo = checkout scm
      def gitCommit = myRepo.GIT_COMMIT

      stage('Build application') {
        container('clojure') {
            sh "lein do clean, ring uberjar"
        }
      }

      stage('Build and push Docker images') {
        container('docker') {
        withCredentials([[$class: 'UsernamePasswordMultiBinding',
                             credentialsId: 'docker-hub-credentials',
                             usernameVariable: 'DOCKER_HUB_USER',
                             passwordVariable: 'DOCKER_HUB_PASSWORD']]) {
            sh """
              docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}
              docker build -t johan1a/jwt-auth:${gitCommit} .
              docker build -t johan1a/jwt-auth:latest .
              docker push johan1a/jwt-auth:${gitCommit}
              docker push johan1a/jwt-auth:latest
              """
          }
        }
      }

      stage('Deploy to dev') {
        container('kubectl') {
          sh "kubectl set image deployment jwt-auth jwt-auth=johan1a/jwt-auth:${gitCommit}"
        }
      }
    }
  }
}
