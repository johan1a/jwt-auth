#!/bin/sh -e
lein do clean, ring uberjar
TAG=$(git rev-parse HEAD)
docker build -t johan1a/jwt-auth:${TAG} .
docker build -t johan1a/jwt-auth:latest .
