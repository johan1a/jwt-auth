(ns jwt-auth.core-test
  (:require [cheshire.core :as cheshire]
            [clojure.test :refer :all]
            [jwt-auth.handler :refer :all]
            [ring.mock.request :as mock]
            [clojure.string :as str]))

(defn parse-body [body]
  (cheshire/parse-string (slurp body) true))

(deftest extract-token

  (testing "Can extract token from auth header"
    (is (token-from-headers "bearer token") "token"))

  (testing "Can extract token from auth header (case insensitive)"
    (is (token-from-headers "Bearer token") "token")))

(deftest extract-auth-config

  (testing "Can extract auth-config"
    (is (auth-config) {:privkey "keys/auth_privkey.pem", :passphrase nil, :pubkey "keys/auth_pubkey.pem"})))

(deftest get-the-private-key

  (testing "Can read private key from a file"
    (is (str/starts-with? (.toString (get-private-key (auth-config))) "RSA Private CRT Key"))))

(deftest get-the-public-key

  (testing "Can read public key from a file"
    (is (str/starts-with? (.toString (get-public-key (auth-config))) "RSA Public Key"))))

(deftest create-user

  (testing "Test create user"
    (let [response (app (-> (mock/request :post  "/api/auth/user/")
                            (mock/json-body {:username (str "Helmut #" (rand)) :password "they'll never guess Thi5!"})))]
      (is (= (:status response) 201)))))

(deftest login

  (testing "login user"
    (let [user (str "Helmut #" (rand))
          password (str "they'll never guess Thi5!")
          createUserResponse (app (-> (mock/request :post  "/api/auth/user/")
                                      (mock/json-body {:username user :password password})))
          loginResponse (app (-> (mock/request :post  "/api/auth/login/")
                                 (mock/json-body {:username user :password password})))
          loginResponseBody (parse-body (:body loginResponse))]
      (is (= (:status createUserResponse) 201))
      (is (= (:status loginResponse) 200))
      (is (instance? String (:token loginResponseBody))))))

(deftest login-401

  (testing "Unauthorized login attempt "
    (let [user (str "Doesn't exist!")
          password (str "they'll never guess Thi5!")
          loginResponse (app (-> (mock/request :post  "/api/auth/login/")
                                 (mock/json-body {:username user :password password})))]
      (is (= (:status loginResponse) 401)))))

(deftest verify

  (testing "verify token"
    (let [user (str "Helmut #" (rand))
          password (str "they'll never guess Thi5!")
          createUserResponse (app (-> (mock/request :post  "/api/auth/user/")
                                      (mock/json-body {:username user :password password})))
          loginResponse (app (-> (mock/request :post  "/api/auth/login/")
                                 (mock/json-body {:username user :password password})))
          token (:token (parse-body (:body loginResponse)))
          verifyResponse (app (-> (mock/request :post "/api/auth/verify/")
                                  (mock/header "Authorization" (str "Bearer " token))))]
      (is (= (:status createUserResponse) 201))
      (is (= (:status loginResponse) 200))
      (is (= (:status verifyResponse) 200)))))

(deftest verify-401

  (testing "verify bad token"
    (let [token "not a valid token"
          verifyResponse (app (-> (mock/request :post "/api/auth/verify/")
                                  (mock/header "Authorization" (str "Bearer " token))))]
      (is (= (:status verifyResponse) 401)))))
