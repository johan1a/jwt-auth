(ns jwt-auth.handler
  (:require [compojure.api.sweet :refer :all]
            [schema.core :as s]
            [buddy.hashers :as hs]
            [buddy.core.keys :as ks]
            [clj-time.core :as t]
            [buddy.sign.jwt :as jws]
            [clojure.java.io :as io]
            [clojure.java.jdbc :as sql]
            [clojure.java.jdbc :as sql]
            [jwt-auth.models.credentials :as credentials]
            [jwt-auth.migration :as migration]
            [clojure.tools.logging :as log]
            [clojure.string :as str]))

(s/defschema TokenBody
  {:token s/Str})

(s/defschema Verification
  {:status s/Str})

(s/defschema Login
  {:username s/Str :password  s/Str})

(defn token-from-headers [authorization]
  (str/trim (second (str/split authorization #" "))))

(def default-password "hunter2")

(defn auth-config []
  {:privkey "keys/auth_privkey.pem" :passphrase (or (System/getenv "PRIVKEY_PASSWORD") default-password)
   :pubkey "keys/auth_pubkey.pem"})

(defn get-private-key [auth-conf]
  (ks/private-key
   (:privkey auth-conf)
   (:passphrase auth-conf)))

(defn get-public-key [auth-conf]
  (ks/public-key
   (:pubkey auth-conf)))

(def expiration-hours (or (System/getenv "EXPIRATION_HOURS") 3))

(defn auth-user [credentials]
  (let [user (credentials/find-user credentials)
        unauthed [false {:message "Invalid username or password"}]]
    (if user
      (if (hs/check (:password credentials) (:password user))
        [true {:user (dissoc user :password)}]
        unauthed)
      unauthed)))

(defn create-auth-token [auth-conf credentials]
  (let [[ok? res] (auth-user credentials)
        exp (-> (t/plus (t/now) (t/hours expiration-hours)))]
    (if ok?
      [true {:token (jws/sign res
                              (get-private-key auth-conf)
                              {:alg :rs256 :exp exp})}]
      [false res])))

(defn verify-token [auth-conf token]
  (try (jws/unsign token (get-public-key auth-conf) {:alg :rs256})
       (catch clojure.lang.ExceptionInfo e false)))

(def app
  (do
    (migration/migrate)
    (api
     {:swagger
      {:ui "/"
       :spec "/swagger.json"
       :data {:info {:title "Jwt-auth"
                     :description "Compojure Api example"}
              :tags [{:name "api", :description "some apis"}]}}}

     (context "/api/auth" []
       :tags ["api"]

       (POST "/user/" []
         :return Verification
         :body [login Login]
         :summary "Creates a new user"
         (do
           (credentials/create login)
           (println (str "Created user: " (:username login)))
           {:status 201 :body {:status "OK"}}))

       (POST "/login/" []
         :return TokenBody
         :body [login Login]
         :summary "Returns a new claim"
         (do
           (let [[ok? res] (create-auth-token (auth-config) login)]
             (if ok?
               (do
                 (println (str "User logged in: " (:username login)))
                 {:status 200 :body {:token (:token res)}})
               (do
                 (println (str "Access denied for user: " (:username login)))
                 {:status 401 :token "Unauthorized"})))))

       (POST "/verify/" []
         :header-params [authorization :- s/Str]
         :return Verification
         :summary "Verifies a claim"
         (do
           (if (verify-token (auth-config) (token-from-headers authorization))
             {:status 200 :body {:status "OK"}}
             {:status 401 :body {:status "Unauthorized"}})))))))
