(ns jwt-auth.models.credentials
  (:require [clojure.java.jdbc :as sql]
            [jwt-auth.database :as db]
            [buddy.hashers :as hs]
            [clojure.tools.logging :as log]
            ))


(defn all []
  (into [] (sql/query db/conn ["select * from credentials order by id desc"])))

(defn find-user [credentials]
    (into {} (sql/query db/conn ["select * from credentials where username = ?" (:username credentials)])))

(defn create [credentials]
  (sql/insert! db/conn :credentials
               [:username :password]
               [(:username credentials)
                (hs/derive (:password credentials))]))
