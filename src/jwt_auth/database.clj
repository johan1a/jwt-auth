(ns jwt-auth.database)

(def conn
  {:dbtype "postgresql"
   :host (or (System/getenv "DB_HOST") "127.0.0.1")
   :dbname (or (System/getenv "DB_NAME") "")
   :user (or (System/getenv "DB_USER") "postgres")
   :password (or (System/getenv "DB_PASSWORD") "postgres")
   :port (or (System/getenv "DB_PORT") "5432")})
