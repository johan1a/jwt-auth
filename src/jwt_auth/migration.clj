(ns jwt-auth.migration
  (:require [clojure.java.jdbc :as sql]
            [jwt-auth.models.credentials :as credentials]
            [jwt-auth.database :as db]
            [clojure.tools.logging :as log]
            ))

(defn migrated? []
  (-> (sql/query db/conn
                 [(str "select count(*) from information_schema.tables "
                       "where table_name='credentials'")])
      first :count pos?))

(defn migrate []
  (when (not (migrated?))
    (log/info "Creating database structure...") (flush)
    (sql/db-do-commands db/conn
                        (sql/create-table-ddl
                         :credentials
                         [[:id :serial "PRIMARY KEY"]
                         [:username :varchar "UNIQUE" "NOT NULL"]
                         [:password :varchar "NOT NULL"]
                         [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]]))
    (log/info "Migration finished")))
