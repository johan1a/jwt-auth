 (defproject jwt-auth "0.1.0-SNAPSHOT"
   :description "FIXME: write description"
   :dependencies [[org.clojure/clojure "1.10.1"]
                  [metosin/compojure-api "1.1.13"]
                  [buddy "2.0.0"]
                  [buddy/buddy-sign "2.2.0"]
                  [org.clojure/java.jdbc "0.7.11"]
                  [org.postgresql/postgresql "9.4-1201-jdbc41"]
                  [org.clojure/tools.logging "0.5.0"]
                  [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                     javax.jms/jms
                                                     com.sun.jmdk/jmxtools
                                                     com.sun.jmx/jmxri]]]
   :ring {:handler jwt-auth.handler/app :port 3001}
   :uberjar-name "server.jar"
   :profiles {:dev {:dependencies [[javax.servlet/javax.servlet-api "3.1.0"]
                                  [cheshire "5.9.0"]
                                  [ring/ring-mock "0.4.0"]]
                   :plugins [[lein-ring "0.12.5"]]}})
