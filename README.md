# jwt-auth

FIXME


## Usage

### Run the application locally


create role jwt_auth_app_dev with login password 'hunter2';
create database jwt_auth_dev
grant all privileges on database jwt_auth_dev to jwt_auth_app_dev;
\c jwt_auth_dev
grant all privileges on all tables in schema public to jwt_auth_app_dev ;

mkdir keys
openssl genrsa -aes256 -out keys/auth_privkey.pem 4096
openssl rsa -pubout -in keys/auth_privkey.pem -out keys/auth_pubkey.pem


Extract public key in DER format (so Java can read it)

openssl rsa -in keys/auth_privkey.pem -pubout -outform DER -out keys/auth_pubkey.der


kubectl create configmap jwt-auth-config --from-file=auth_privkey.pem=keys/auth_privkey.pem,auth_pubkey.pem=keys/auth_pubkey.pem

kubectl apply -f secrets_dev.yml
kubectl apply -f jwt_auth.yml

`
set -x PRIVKEY_PASSWORD hunter2
lein ring server [port]
`

### Use the repl
lein repl
(use 'jwt-auth.handler)

### require lib in repl
(require '[clojure.string :as str])

### Run the tests

`lein test`

### Packaging and running as standalone jar

```
lein do clean, ring uberjar
java -jar target/server.jar
```

### Packaging as war

`lein ring uberwar`



## License

Copyright ©  FIXME

