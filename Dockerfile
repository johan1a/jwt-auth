FROM java:8-alpine

WORKDIR /app

RUN mkdir /app/log

EXPOSE 3000

COPY target/server.jar /app/server.jar

CMD ["java", "-jar", "/app/server.jar"]
